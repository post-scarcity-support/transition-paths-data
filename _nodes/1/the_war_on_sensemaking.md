---
tags:
- Learning sense making
- Meaning crisis
- Scientific method

order: 100
name: "The War on Sensemaking"
layout: node
excerpt: "What can we trust? Why is the 'information ecology' so damaged, and what would it take to make it healthy?"

---

# The War on Sensemaking, Daniel Schmachtenberger

> This is a fundamental question, because without good sensemaking, we cannot even begin to act in the world. It is also a central concern in what many are calling the "meaning crisis", because what is meaningful is connected to what is real.

> Daniel Schmachtenberger is an evolutionary philosopher - his central interest is civilization design: developing new capacities for sense-making and choice-making, individually and collectively, to support conscious sustainable evolution.

https://www.youtube.com/watch?v=7LqaotiGWjQ

# How important is it?


---
order: 160
tags:
- Green movement
- Sustainability
name: Is aiming at sustainability enough
excerpt: ''
img: https://miro.medium.com/max/2560/1*A0fq5BKMFj-34Q94vY6XqA.jpeg
layout: node

---
# Is aiming at sustainability enough

Read this Medium article by Daniel C. Wahl [Sustainability is not enough: we need regenerative cultures](https://medium.com/@designforsustainability/sustainability-is-not-enough-we-need-regenerative-cultures-4abb3c78e68b "Medium article: Sustainability is not enough: we need regenerative cultures")

> Sustainability alone is not an adequate goal. The word sustainability itself is inadequate, as it does not tell us what we are actually trying to sustain. In 2005, after spending two years working on my doctoral thesis on design for sustainability, I began to realize that what we are actually trying to sustain is the underlying pattern of health, resilience and adaptability that maintain this planet in a condition where life as a whole can flourish. Design for sustainability is, ultimately, design for human and planetary health (Wahl, 2006b).

# How important is it?


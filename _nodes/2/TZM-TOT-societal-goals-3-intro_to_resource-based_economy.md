---
layout: node

tags:
- TZM Train of thought
  
# post title
name: An Intro. to a Resource-Based Economy [ TEDx - Peter Joseph ]

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
order: 244
---

https://www.youtube.com/embed/6AysJYwlu0k

# How important is it?


---
tags:
- Be the change

order: 320
name: Steps in creating the transition
layout: node
excerpt: ''

---

0 - Typical stuff people find important

1 - Accepting we are in trouble (Humanity isn't capable of making collective decision to save the planet) 

2 - Realizing there is something more (Systems thinking, etc)

3 - Accepting that you need to make it happen

4 - Re-organizing your life 

5 - Building the future

6 - RBE

# How important is it?


---
layout: node

# post title
name: "The Zeitgeist Movement Defined, Essay 1: Overview"

tags:
- TZM activism
- TZM Train of thought

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
order: 20

excerpt: "Read The Zeitgeist Movement Defined, Essay 1: Overview"

---

At first, we recommend to read the first essay of the book The Zeitgeist Movement Defined. In this text, the core of TZM is presented in a well structured way. We also encourage you to read the whole book!

PDF https://drive.google.com/file/d/0By5SSxpw6TMIR3hjOGRQVy1zUjQ/view

# How important is it?


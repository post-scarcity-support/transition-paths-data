---
tags:
- healing from consumerism
- Self improvement and healing
order: 10
name: Think about why you want to buy things
layout: node
excerpt: 'Finding out what affects your willingness to take part in overconsumption'
img: https://thebetterplan.files.wordpress.com/2014/08/u8toqit.jpg
---
# The Negative Effects of Consumerism

You feel sad. Nature is getting destroyed.

# What causes consumerism

Marketing. PR.

# How to start getting better

Stop buying useless stuff

# More

Read this https://greentumble.com/the-negative-effects-of-consumerism/

# How important is it?


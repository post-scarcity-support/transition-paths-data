---
layout: node

tags:
- Learning sense making
- Scientific method

# post title
name: How Does Do Science? │ Figuring out what's true



# hide post
hidden: false

# class values
type_class: true
order: 1

img: https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/The_Scientific_Method_as_an_Ongoing_Process.svg/1200px-The_Scientific_Method_as_an_Ongoing_Process.svg.png
---

https://www.youtube.com/embed/3MRHcYtZjFY

# How important is it?


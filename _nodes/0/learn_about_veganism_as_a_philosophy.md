---
tags:
- Living by princibles
- Compassion

order: 40
name: Learn about veganism as a philosophy
layout: node
excerpt: ''

---
# Simply put

Veganism is all about reducing unnecessary suffering. Vegans extend the concept of suffering to all animals including humans. We accept that understanding the suffering we create and compassion we feel towards all beings are more important reasons to make decisions in life than our slight discomfort, taste buds or habits.

Understanding that the current state of humanity’s sociocultural evolution is creating structures of societies and thought that lead to destroying earth’s ability to support life and thus extreme suffering, we choose to support people move past their old ways of thinking and help them find compassion and happiness.

# Meat eaters case for veganism

> What makes this interesting is that Alex is not vegan himself, but he clearly states that the reasons discussed in the video will eventually lead him to adopt a vegan lifestyle if no one can prove his arguments wrong.

https://www.youtube.com/watch?v=C1vW9iSpLLk

## Here is short article about the arguments presented in the video

https://www.theodysseyonline.com/meat-eaters-case-for-veganism

# Moral Vegetarianism

https://plato.stanford.edu/entries/vegetarianism/

# How important is it?

Veganism is part of learning to accept arguments based on reason and ethics, and deciding to live by them.

It cannot solve the systemic problems that create a lot of suffering in the world, but it is an easy way to take a step forward.



---
tags:
- TZM activism

order: 20
name: Watch "What is the zeitgeist movement"
layout: node
excerpt: ''
img: https://www.forewordreviews.com/books/covers/the-zeitgeist-movement-defined.w250.h140.jpg
---
The Zeitgeist Movement is a global sustainability activist group working to bring the world together for the common goal of species sustainability before it is too late. Divisive notions such as nations, governments, races, political parties, religions, creeds or class are non-operational distinctions in the view of The Movement. Rather, we recognize the world as one system and the human species as a singular unit, sharing a common habitat.

https://www.youtube.com/watch?v=PJaCB3mxXE0

LIKE The Zeitgeist Movement @ [https://www.facebook.com/tzmglobal](https://www.facebook.com/tzmglobal "https://www.facebook.com/tzmglobal") FOLLOW The Zeitgeist Movement @ [https://twitter.com/tzmglobal](https://twitter.com/tzmglobal "https://twitter.com/tzmglobal") JOIN THE MAILING LIST: [http://www.thezeitgeistmovement.com/](http://www.thezeitgeistmovement.com/ "http://www.thezeitgeistmovement.com/")

# How important is it?


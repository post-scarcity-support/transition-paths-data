---
tags:
- Integral community
- Learning sense making
order: 40
name: Integral theory 4Qs
layout: node
excerpt: ''

---
# Learn about Integral theory 4Qs

I AM - David Long introduces you to the 4 quadrants of Integral theory (1 of the 5 main lenses of an AQAL) and gives you examples of how understanding these distinctions can be applied in the world.

https://www.youtube.com/watch?v=3y2lmeRKIl0

# How important is it?


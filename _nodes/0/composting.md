---
tags:
- Green movement
- self-sustainability
order: 50
name: Composting
layout: node
excerpt: ''

---
# Why composting?

Everybody has to eat after all so you will always create some food waste. If you don't have food waste collection in your area, you should consider starting composting.

[https://www.recyclenow.com/reduce-waste/composting/why-compost](https://www.recyclenow.com/reduce-waste/composting/why-compost "https://www.recyclenow.com/reduce-waste/composting/why-compost")

> When waste is sent to landfill, air cannot get to the organic waste. Therefore as the waste breaks down it creates a harmful greenhouse gas, methane, which damages the Earth's atmosphere. However, when this same waste is composted above ground at home, oxygen helps the waste to decompose aerobically which means hardly any methane is produced, which is good news for the planet. And what's more, after nine to twelve months, you get a free fertiliser for your garden and plant pots to keep them looking beautiful.

# How?

You can find good instructions on this site [https://www.recyclenow.com/reduce-waste/composting](https://www.recyclenow.com/reduce-waste/composting "https://www.recyclenow.com/reduce-waste/composting"). 

# How important is it?

It is an easy way to reduce your climate impact. It is also a great way of being more connected to how nutrients and carbon circle in nature.


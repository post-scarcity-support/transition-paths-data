---
layout: node

# post title
name: "Peter Joseps's brief talk and Q&A about Train of Thought of TZM"

tags:
- TZM activism
- TZM Train of thought

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
order: 26

excerpt: ""
---

# Peter Joseph 'Train of Thought' + Q & A Session (ZDAY 2017 Brisbane Australia)

https://www.youtube.com/embed/24Px_-9rU1A

# Another great talk of Peter Joseph about The New Human Right Movement

https://youtu.be/GvkchZADaaA

# How important is it?


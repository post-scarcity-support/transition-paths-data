---
tags:
- Open source movement
- Internet privacy

order: 1
name: Learn what is Free/Libre and Open Source Software
layout: node
excerpt: ''

---

> To emphasize that “free software” refers to freedom and not to price, we sometimes write or say “free (libre) software,” adding the French or Spanish word that means free in the sense of freedom. In some contexts, it works to use just “libre software.”

https://www.gnu.org/philosophy/floss-and-foss.en.html

# How important is it?


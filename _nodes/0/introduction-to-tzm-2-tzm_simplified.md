---
layout: node

# post title
name: "TZM Simplified by Franky Müller"

tags:
- TZM activism
- TZM Train of thought

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
order: 22

excerpt: ""
---

Franky Müller describes The Zeitgeist Movement [TZM] in a simplified manner. A breakdown of the core, specific principles of TZM's proposals in a way everyone should understand. The talk was held at the 7th Annual Zeitgeist Day [ZDay] in Berlin, Germany, March 14th 2015.

https://www.youtube.com/embed/uPnwvs-AyOs

# How important is it?


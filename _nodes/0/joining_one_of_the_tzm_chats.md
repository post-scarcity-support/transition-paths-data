---
tags:
- TZM activism
order: 20
name: Joining one of the TZM chats
layout: node
excerpt: ''
img: https://transition-paths-data.tzm.community/img/tzm_discord.png

---
# Connect with people

Join one of the chats/groups, introduce youself, have great discussions and maybe make new friends.

# Matrix/Riot

## Matrix community

This [Matrix community](https://matrix.to/#/+tzm:matrix.tzm.community "Matrix community") is where all channels are listed.

## TZMC - General channel

TZMC-General channel on matrix.tzm.community server is connected to Discord servers #general -channel. Follow this [link](https://matrix.to/#/!dlckiTiXfrYDXbQoaK:matrix.tzm.community?via=matrix.tzm.community "Matrix general channel").

# TZM Discord server

https://discord.gg/q6Hhku

# Telegram

## General discussion

Telegram TZM Discussion https://t.me/joinchat/GH6U60aoLtN9Pzgzpggd0w

# How important is it?


---
tags:
- TZM activism
order: 30
name: Join monthly TZM online assemblies
layout: node
excerpt: 'There is two meetings every month to make it easier for people in around the globe'

---

# How to connect

## Saturday meetups

The last Saturday of every month at 9:00 UTC in the (voice) International channel. On Discord ( https://discord.gg/uGKvWqz )

## Wednesday meetups

The last Wednesday of every month at 21:00 UTC on Mumble

### Mumble Server Details

Download: mumble.info
Server: zeitgeistmovement.cz
Password: NasMumble*2018

Confirm the unverified certificate popup

Server provided by provided by Czech chapter.

# Everyone is welcome!

Everyone is invited and if it’s not possible to attend then you can read the minutes.

Note: The old Team Speak server is no longer available.

The UK chapter also regularly meets up on Discord if you are interested.

# How important is it?


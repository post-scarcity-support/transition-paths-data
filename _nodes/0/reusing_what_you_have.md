---
tags:
- Green movement
- self-sustainability
- Living by princibles
order: 40
name: Re-using things you have
layout: node
excerpt: ''

---
# Re use items whenever possible instead of one-time things (e.g. bring your own bag to a grocery store instead of buying a plastic one on the spot)

# How important is it?


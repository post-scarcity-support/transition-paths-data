var fs = require('fs');
const { promisify } = require('util')

const readFileAsync = promisify(fs.readFile)
const writeFileAsync = promisify(fs.writeFile)
const readdir = promisify(fs.readdir)

var info = ""

function log(text) {
	console.log(info + " --- " + text);
}

async function writeNodes(path) {
	var files = await readdir("_nodes/" + path);
	for(i in files) {
		var file = files[i];
		if(!file.includes("json") && file.includes(".md") && !file.includes("gitignore")) {
			info = file;
			log("file " + file);
			var fdata = await readFileAsync("_nodes/" + path + "/" + file, {encoding: 'utf-8'});
			log('received data: ' + fdata);
			
			var ndata = ""
			var lines = fdata.split("\n");
			for(iline in lines) {
				var line = lines[iline];
				if(line.indexOf("order:")>=0) {
					line = line.replace(/\"/g, "");
					//var order = line.split(":")[1].trim();
					//var norder = order * 10;
					//log("order " + order + " to " + norder);
					//line = "order: " + norder;
				}
				
				ndata += line + "\n";
			}
			
			if(ndata.indexOf("# How important is it?")<0) {
				ndata += "\n\n# How important is it?\n\n"; 
			}
			
			ndata = ndata.replace(/\n\n\n/g, "\n\n");
			await writeFileAsync("_nodes/" + path + "/" + file, ndata);
			log("file " + file + " done!");
		}
	};
	
	return files;
}

async function process() {
	await writeNodes("0");
	await writeNodes("1");
	await writeNodes("2");
	await writeNodes("3");
	await writeNodes("4");
	await writeNodes("5");
	await writeNodes("6");
}

process().then(() => {
	log("all done");
});

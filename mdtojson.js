var fs = require('fs');
var marked = require('marked');

const { promisify } = require('util')
const YAML = require('yaml')

const readFileAsync = promisify(fs.readFile)
const writeFileAsync = promisify(fs.writeFile)
const readdir = promisify(fs.readdir)

var alldata = {};

var info = ""

function log(text) {
	console.log(info + " --- " + text);
}

function parseParagraph(np, p, status) {
	var spart = p.text;
	// spart = spart.trim();
	log("paragraph text:" + spart);
	if(spart.length>1) {
		var paragraph = { type: "paragraph" };
		if(status.blockquote) {
			paragraph.type = "blockquote";
		} 
		
		var parts = [];

		var splitted = spart.split("[");
		for(i in splitted) {
			var text = splitted[i]
			log("splitted " + text);
			
			if(text.indexOf("](")>0) {
				var linklist = text.split("(");
				log("FOUND A LINK " + linklist);
				var linkname = linklist[0].replace(/]/, "");
				var linkdest = linklist[1].split(" ")[0];
				var linkalt = linklist[1].substr(linkdest.length);
				linkalt = linkalt.replace(/\"/g, "").replace(")", "");
				log("FOUND A LINK name:" + linkname + " linkdest " + linkdest + " linkalt:" + linkalt);
				var part = { 
						type: "link",
						href: linkdest,
						alt: linkalt,
						text: linkname
				};
				parts.push(part);
			}  else {
				if(i>=1) {
					text = "[" + text;
				}
				
				var part = {};
				part.text = text;
				if(text.indexOf("https://") == 0 && (text.indexOf("youtube.com")>0 || text.indexOf("youtu.be")>0)) {
					var ieq = text.lastIndexOf("=");
					var islash = text.lastIndexOf("/");
					var cutindex = ieq>islash? ieq : islash;
					var video_id = text.substr(cutindex+1);
	
					var ampersandPosition = video_id.indexOf('&');
					if(ampersandPosition != -1) {
					  video_id = video_id.substring(0, ampersandPosition);
					}
	
					part.type = "youtube";
					part.video_id = video_id;
				} else {
					part.type = "text";
				}
				parts.push(part);
			}			
			
			paragraph.parts = parts;
		}
		
		np.push(paragraph);
	}
	return np;
}

function parseJSON(list, level, o) {
	var status = {};
	
	for(ititle in o) {
		log("title index " + ititle);
		var p = o[ititle];
		
		log("p " + JSON.stringify(p));
		
		if(p.type=="paragraph") {
			parseParagraph(list, p, status);
		} else if(p.type=="blockquote_start") {
			status.blockquote = true
		} else if(p.type=="blockquote_end") {
			status.blockquote = false
		} else if(p.type=="space") {
			//
		} else {			
			list.push(p);
		}
	}
}

async function writeNodes(alldata, path) {
	var files = await readdir("_nodes/" + path);
	for(i in files) {
		var file = files[i];
		if(!file.includes("json") && file.includes(".md") && !file.includes("gitignore")) {
			info = file;
			log("file " + file);
			var fdata = await readFileAsync("_nodes/" + path + "/" + file, {encoding: 'utf-8'});
			log('received data: ' + fdata);

			var nyaml = YAML.parseAllDocuments(fdata)[0];
			log("yaml " + JSON.stringify(nyaml));
			nyaml = JSON.parse(JSON.stringify(nyaml));
			
			var smd = fdata.substr(fdata.indexOf("---", 4)+3);
			log("smd " + smd);

			var o = marked.lexer(smd);
			log("marked : " + JSON.stringify(o));

			var no = [];
			
			parseJSON(no, 1, o);
			
			var j = JSON.stringify(no);
			log("json " + j);

			var ndata = {};
			ndata.id = file.replace("\.md", "");
			ndata.name = nyaml.name;
			ndata.order = nyaml.order;
			ndata.tags = nyaml.tags;
			ndata.img = nyaml.img;
			ndata.path = path;
			ndata.excerpt = nyaml.excerpt;
			alldata[ndata.id] = ndata;
			log("alldata storing " + JSON.stringify(ndata));
		
			await writeFileAsync("json/" + (file.substr(0, file.indexOf("."))) + ".json", j);
			log("file " + file + " done!");
		}
	};
	
	return files;
}

async function writeAll(alldata) {
	await writeFileAsync("data.json", JSON.stringify(alldata));
	log("data.json done!");
}

async function process(alldata) {
	await writeNodes(alldata, "0");
	await writeNodes(alldata, "1");
	await writeNodes(alldata, "2");
	await writeNodes(alldata, "3");
	await writeNodes(alldata, "4");
	await writeNodes(alldata, "5");
	
	await writeAll(alldata);
}

process(alldata).then(() => {
	log("all done");
});

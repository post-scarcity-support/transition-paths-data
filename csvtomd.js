var md2json = require('md-2-json');
var fs = require('fs');
var parse = require("csv-parse")

const parser = parse({
  delimiter: ',',
  columns: true
})

parser.on('readable', () => {
	var record;
	
	while (record = parser.read()) {
		console.log("record " + JSON.stringify(record));
		var id = record.name.replace(/[&\/\\#,+()$~%.'":*?<>{} ]/g, '_').toLowerCase();
		var order = record.order;
		var tags = [];
		for(i in record) {
			if(record[i].toLowerCase() == "x") {
				var tag = i.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
				console.log("tag: " + tag);
				tags.push(tag);
			}
		}

		if(!fs.existsSync("_nodes/" + id + ".md")) {
			console.log("creating file id:" + id + " order:" + order);
			var content = "---\n"
			content += "tags:\n";
			for(tag in tags) {
				content += "- " + tags[tag] + "\n";
			}
			content += "order: " + order + "\n";
			content += "name: " + record.name + "\n";
			content += "layout: node\n";
			content += "---\n";
			content += "# " + record.name;
			content += "\n\n";
			console.log("content: " + content);
			fs.writeFileSync("_nodes/" + id + ".md", content);
		};
	}
});


  fs.readFile("data.csv", {encoding: 'utf-8'}, function(err,data){
    if (!err) {
        console.log('received data: ' + data);
        parser.write(data);
    } else {
        console.log(err);
    }
});	    
	  


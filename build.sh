export BUNDLE_PATH=.vendor
bundle install --path .vendor/bundle

npm install
node mdtojson.js

GEM_PATH=${GEM_PATH}:.vendor bundle exec jekyll build
GEM_PATH=${GEM_PATH}:.vendor bundle exec jekyll $*

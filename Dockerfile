FROM docker.io/library/node

WORKDIR /project

ADD package.json /project
ADD package-lock.json /project
RUN npm install

RUN find
ADD *.js /project/

ADD _nodes /project/_nodes
ADD json /project/json
RUN node fixmd.js
RUN node mdtojson.js

FROM docker.io/library/ruby:2.7

RUN gem install bundler

WORKDIR /project

ADD Gemfile /project/
ADD Gemfile.lock /project/
RUN bundle install

COPY --from=0 /project /project

ADD _layouts /project
ADD _nodes /project
ADD img /project
ADD scripts /project

RUN jekyll build -d public

RUN find | grep -v node_mod | grep -v git


